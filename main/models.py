from django.contrib.auth import models as auth_models
from django.db import models
from django.urls import reverse

from main import const


class CustomerManager(auth_models.UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(type=const.CHOICE_CUSTOMER)


class DirectorManager(auth_models.UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(type=const.CHOICE_DIRECTOR)


class ClientManager(auth_models.UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(type=const.CHOICE_CLIENT)


class Post(models.Model):
    """
    Должность
    """
    name = models.CharField(
        verbose_name="Наименование",
        max_length=1024,
        null=False, blank=False
    )
    cost = models.FloatField(
        verbose_name="Оклад",
        null=False, blank=False
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('post_detail', args=(self.pk,))


class CustomUser(auth_models.User):
    """
    Пользователь
    """
    customers = CustomerManager()
    directors = DirectorManager()
    clients = ClientManager()

    type = models.IntegerField(
        verbose_name="Тип",
        choices=const.USER_CHOICES,
        null=False, blank=False,
    )
    fio = models.CharField(
        verbose_name="ФИО",
        max_length=1024,
        null=False, blank=False,
    )
    dob = models.DateField(
        verbose_name="Дата рождения",
        null=True, blank=True,
    )
    passport = models.CharField(
        verbose_name="Паспорт",
        max_length=1024,
        null=True, blank=False,
    )
    number = models.CharField(
        verbose_name="Номер телефона",
        max_length=1024,
        null=False, blank=False
    )
    post = models.ForeignKey(
        verbose_name="Должность",
        to=Post,
        related_name="customers",
        on_delete=models.SET_NULL,
        null=True, blank=True,
    )

    @property
    def is_director(self):
        return self.type == const.CHOICE_DIRECTOR

    @property
    def is_customer(self):
        return self.type == const.CHOICE_CUSTOMER

    @property
    def is_client(self):
        return self.type == const.CHOICE_CLIENT

    def __str__(self):
        return self.passport

    def get_absolute_url(self):
        return reverse('customer_detail', args=(self.pk,))


class Order(models.Model):
    """
    Заказ
    """
    name = models.CharField(
        verbose_name="Наименование",
        max_length=1024,
        null=False, blank=False
    )
    status = models.IntegerField(
        verbose_name="Статус",
        choices=const.STATUS_CHOICES,
        null=False, blank=False,
        default=const.STATUS_NONE
    )
    cost = models.FloatField(
        verbose_name="Цена",
        null=False, blank=False
    )
    date_delivery = models.DateField(
        verbose_name="Дата доставки",
        null=True, blank=True,
    )
    date_start = models.DateField(
        verbose_name="Дата начала",
        null=False, blank=False,
    )
    date_finish = models.DateField(
        verbose_name="Дата окончания",
        null=True, blank=True,
    )
    address = models.CharField(
        verbose_name="Адрес",
        max_length=1024,
        null=False, blank=False
    )
    customer = models.ForeignKey(
        verbose_name="Сотрудник",
        to=CustomUser,
        related_name="custoemr_orders",
        on_delete=models.SET_NULL,
        null=True, blank=False,
    )
    client = models.ForeignKey(
        verbose_name="Клиент",
        to=CustomUser,
        related_name="client_orders",
        on_delete=models.SET_NULL,
        null=True, blank=False,
    )

    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('order_detail', args=(self.pk,))


class OrderClient(Order):
    class Meta:
        proxy = True

    def get_absolute_url(self):
        return reverse('order_cl_detail', args=(self.pk,))


class CategoryFurniture(models.Model):
    """
    Категория мебели
    """
    name = models.CharField(
        verbose_name="Наименование",
        max_length=1024,
        null=False, blank=False
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category_furniture_detail', args=(self.pk,))


class Factory(models.Model):
    """
    Завод
    """
    name = models.CharField(
        verbose_name="Наименование",
        max_length=1024,
        null=False, blank=False
    )
    inn = models.IntegerField(
        verbose_name="ИНН",
        null=False, blank=False,
    )
    address = models.CharField(
        verbose_name="Адрес",
        max_length=1024,
        null=False, blank=False
    )
    number = models.CharField(
        verbose_name="Номер телефона",
        max_length=1024,
        null=False, blank=False
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('factory_detail', args=(self.pk,))


class Furniture(models.Model):
    """
    Мебель
    """
    name = models.CharField(
        verbose_name="Наименование",
        max_length=1024,
        null=False, blank=False
    )
    cost = models.FloatField(
        verbose_name="Цена",
        null=False, blank=False
    )
    count = models.IntegerField(
        verbose_name="Количество",
        null=False, blank=False,
    )
    category = models.ForeignKey(
        verbose_name="Категория",
        to=CategoryFurniture,
        related_name="furniture",
        on_delete=models.SET_NULL,
        null=True, blank=False,
    )
    factory = models.ForeignKey(
        verbose_name="Завод",
        to=Factory,
        related_name="furniture",
        on_delete=models.SET_NULL,
        null=True, blank=False,
    )
    order = models.ForeignKey(
        verbose_name="Заказ",
        to=Order,
        related_name="furniture",
        on_delete=models.CASCADE,
        null=False, blank=False,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('furniture_detail', args=(self.pk,))
