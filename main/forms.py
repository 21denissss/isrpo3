from django import forms
from django.contrib.auth.forms import UserCreationForm

from main import const
from main.models import CustomUser, Factory, Post, CategoryFurniture, Furniture, Order


class CustomRegistrationForm(UserCreationForm):
    type = forms.ChoiceField(
        choices=const.USER_CHOICES,
        label='Тип пользователя',
    )

    class Meta(UserCreationForm.Meta):
        model = CustomUser

        fields = (
            'username',
            'password1',
            'password2',
            'email',
            'fio',
            'passport',
            'dob',
            'type',
        )


class FactoryForm(forms.ModelForm):
    class Meta:
        model = Factory
        fields = (
            'name',
            'inn',
            'address',
            'number',
        )


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = (
            'name',
            'cost',
        )


class CategoryFurnitureForm(forms.ModelForm):
    class Meta:
        model = CategoryFurniture
        fields = (
            'name',
        )


class FurnitureForm(forms.ModelForm):
    class Meta:
        model = Furniture
        fields = (
            'name',
            'cost',
            'count',
            'category',
            'factory',
            'order',
        )


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = (
            'name',
            'status',
            'cost',
            'date_start',
            'address',
            'client',
        )

    def __init__(self, customer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.instance.customer = customer
        self.fields['client'].queryset = CustomUser.clients.all()


class OrderClientForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = (
            'status',
        )
