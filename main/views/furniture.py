from django.urls import reverse
from django.views.generic import ListView, CreateView, DetailView, DeleteView

from main.forms import FurnitureForm
from main.models import Furniture


class FurnitureListView(ListView):
    model = Furniture
    template_name = "furniture/list.html"


class FurnitureCreateView(CreateView):
    form_class = FurnitureForm
    template_name = "furniture/create.html"


class FurnitureDetailView(DetailView):
    model = Furniture
    template_name = "furniture/detail.html"


class FurnitureDeleteView(DeleteView):
    model = Furniture

    @property
    def success_url(self):
        return reverse('furniture_list')

