from django.urls import reverse
from django.views.generic import ListView, CreateView, DetailView, DeleteView

from main.forms import CategoryFurnitureForm
from main.models import CategoryFurniture


class CategoryFurnitureListView(ListView):
    model = CategoryFurniture
    template_name = "category_furniture/list.html"


class CategoryFurnitureCreateView(CreateView):
    form_class = CategoryFurnitureForm
    template_name = "category_furniture/create.html"


class CategoryFurnitureDetailView(DetailView):
    model = CategoryFurniture
    template_name = "category_furniture/detail.html"


class CategoryFurnitureDeleteView(DeleteView):
    model = CategoryFurniture

    @property
    def success_url(self):
        return reverse('category_furniture_list')

