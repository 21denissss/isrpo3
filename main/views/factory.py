from django.urls import reverse
from django.views.generic import ListView, CreateView, DetailView, DeleteView

from main.forms import FactoryForm
from main.models import Factory


class FactoryListView(ListView):
    model = Factory
    template_name = "factory/list.html"


class FactoryCreateView(CreateView):
    form_class = FactoryForm
    template_name = "factory/create.html"


class FactoryDetailView(DetailView):
    model = Factory
    template_name = "factory/detail.html"


class FactoryDeleteView(DeleteView):
    model = Factory

    @property
    def success_url(self):
        return reverse('factory_list')

