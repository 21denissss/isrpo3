from django.urls import reverse
from django.views.generic import ListView, DetailView, UpdateView

from main.models import CustomUser


class CustomerListView(ListView):
    model = CustomUser
    template_name = "customer/list.html"


class CustomerDetailView(DetailView):
    model = CustomUser
    template_name = "customer/detail.html"


class CustomerUpdateView(UpdateView):
    model = CustomUser
    fields = ['post']
    template_name = "customer/update.html"

    @property
    def success_url(self):
        return reverse('customer_list')
