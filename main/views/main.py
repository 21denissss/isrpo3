# Create your views here.
from django.views.generic import TemplateView
from registration.backends.simple.views import RegistrationView

from main.forms import CustomRegistrationForm


class IndexView(TemplateView):
    template_name = "main/index.html"


class CustomRegistrationView(RegistrationView):
    form_class = CustomRegistrationForm
    success_url = '/'
