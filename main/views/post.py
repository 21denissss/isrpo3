from django.urls import reverse
from django.views.generic import ListView, CreateView, DetailView, DeleteView

from main.forms import PostForm
from main.models import Post


class PostListView(ListView):
    model = Post
    template_name = "post/list.html"


class PostCreateView(CreateView):
    form_class = PostForm
    template_name = "post/create.html"


class PostDetailView(DetailView):
    model = Post
    template_name = "post/detail.html"


class PostDeleteView(DeleteView):
    model = Post

    @property
    def success_url(self):
        return reverse('post_list')

