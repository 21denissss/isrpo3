from .category_furniture import CategoryFurnitureCreateView, CategoryFurnitureDeleteView, CategoryFurnitureDetailView, \
    CategoryFurnitureListView
from .customer import CustomerListView, CustomerDetailView, CustomerUpdateView
from .factory import FactoryListView, FactoryCreateView, FactoryDetailView, FactoryDeleteView
from .furniture import FurnitureListView, FurnitureCreateView, FurnitureDetailView, FurnitureDeleteView
from .main import IndexView, CustomRegistrationView
from .order import OrderListView, OrderCreateView, OrderDetailView, OrderDeleteView, OrderUpdateView, \
    OrderIrrelevantListView, OrderActualListView, OrderClientDetailView, OrderClientUpdateView
from .post import PostListView, PostCreateView, PostDeleteView, PostDetailView
