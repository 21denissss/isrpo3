from django.contrib.auth import get_user
from django.shortcuts import redirect
from django.urls import reverse
from django.views import View
from django.views.generic import ListView, CreateView, DetailView, DeleteView, UpdateView

from main import const
from main.forms import OrderForm
from main.models import Order, OrderClient


class OrderListView(ListView):
    model = Order
    template_name = "order/customer/list.html"

    def get_queryset(self):
        return Order.objects.filter(customer=get_user(self.request).customuser)


class OrderCreateView(CreateView):
    form_class = OrderForm
    template_name = "order/customer/create.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['customer'] = get_user(self.request).customuser
        return kwargs


class OrderDetailView(DetailView):
    model = Order
    template_name = "order/customer/detail.html"


class OrderDeleteView(DeleteView):
    model = Order

    @property
    def success_url(self):
        return reverse('order_list')


class OrderUpdateView(UpdateView):
    model = Order
    fields = (
        'status',
        'date_delivery',
        'date_finish',
    )
    template_name = "order/customer/update.html"

    @property
    def success_url(self):
        return reverse('order_list')


class OrderIrrelevantListView(ListView):
    model = OrderClient
    template_name = "order/client/list.html"

    def get_queryset(self):
        return OrderClient.objects.filter(
            client=get_user(self.request).customuser,
            status__in=[
                const.STATUS_DONE,
            ]
        )


class OrderActualListView(ListView):
    model = OrderClient
    template_name = "order/client/list.html"

    def get_queryset(self):
        return OrderClient.objects.filter(
            client=get_user(self.request).customuser,
            status__in=[
                const.STATUS_ACCEPTED,
                const.STATUS_STORED,
                const.STATUS_MANUFACTURING,
                const.STATUS_TRANSIT,
                const.STATUS_NONE,
            ]
        )


class OrderClientDetailView(DetailView):
    model = OrderClient
    template_name = "order/client/detail.html"


class OrderClientUpdateView(View):
    def post(self, request, *args, **kwargs):
        order = Order.objects.get(pk=kwargs['pk'])
        order.status = const.STATUS_DONE
        order.save()
        return redirect(order)
