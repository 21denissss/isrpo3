from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('accounts/login/', LoginView.as_view(next_page='/'), name='login'),
    path('accounts/logout/', LogoutView.as_view(next_page='/'), name='logout'),
    path('accounts/registration/', views.CustomRegistrationView.as_view(), name='registration'),

    path('factory/list/', views.FactoryListView.as_view(), name='factory_list'),
    path('factory/create/', views.FactoryCreateView.as_view(), name='factory_create'),
    re_path('factory/detail/(?P<pk>\d+)$', views.FactoryDetailView.as_view(), name='factory_detail'),
    re_path('factory/delete/(?P<pk>\d+)$', views.FactoryDeleteView.as_view(), name='factory_delete'),

    path('post/list/', views.PostListView.as_view(), name='post_list'),
    path('post/create/', views.PostCreateView.as_view(), name='post_create'),
    re_path('post/detail/(?P<pk>\d+)$', views.PostDetailView.as_view(), name='post_detail'),
    re_path('post/delete/(?P<pk>\d+)$', views.PostDeleteView.as_view(), name='post_delete'),

    path('customer/list/', views.CustomerListView.as_view(), name='customer_list'),
    re_path('customer/detail/(?P<pk>\d+)$', views.CustomerDetailView.as_view(), name='customer_detail'),
    re_path('customer/update/(?P<pk>\d+)$', views.CustomerUpdateView.as_view(), name='customer_update'),

    path('category_furniture/list/', views.CategoryFurnitureListView.as_view(), name='category_furniture_list'),
    path('category_furniture/create/', views.CategoryFurnitureCreateView.as_view(), name='category_furniture_create'),
    re_path('category_furniture/detail/(?P<pk>\d+)$', views.CategoryFurnitureDetailView.as_view(), name='category_furniture_detail'),
    re_path('category_furniture/delete/(?P<pk>\d+)$', views.CategoryFurnitureDeleteView.as_view(), name='category_furniture_delete'),

    path('furniture/list/', views.FurnitureListView.as_view(), name='furniture_list'),
    path('furniture/create/', views.FurnitureCreateView.as_view(), name='furniture_create'),
    re_path('furniture/detail/(?P<pk>\d+)$', views.FurnitureDetailView.as_view(), name='furniture_detail'),
    re_path('furniture/delete/(?P<pk>\d+)$', views.FurnitureDeleteView.as_view(), name='furniture_delete'),

    path('order/list/', views.OrderListView.as_view(), name='order_list'),
    path('order/create/', views.OrderCreateView.as_view(), name='order_create'),
    re_path('order/detail/(?P<pk>\d+)$', views.OrderDetailView.as_view(), name='order_detail'),
    re_path('order/delete/(?P<pk>\d+)$', views.OrderDeleteView.as_view(), name='order_delete'),
    re_path('order/update/(?P<pk>\d+)$', views.OrderUpdateView.as_view(), name='order_update'),

    path('order_irr/list/', views.OrderIrrelevantListView.as_view(), name='order_irr_list'),
    path('order_act/list/', views.OrderActualListView.as_view(), name='order_act_list'),
    re_path('order_cl/update/(?P<pk>\d+)$', views.OrderClientUpdateView.as_view(), name='order_cl_update'),
    re_path('order_cl/detail/(?P<pk>\d+)$', views.OrderClientDetailView.as_view(), name='order_cl_detail'),
]
